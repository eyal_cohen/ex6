package oop.ex6.main;

import java.io.File;
import java.io.IOException;
import oop.ex6.parser.Parser;

/*
 * The class that manages the printing to the screen, based on the results of the given files analysis.
 */
class Manager {
	
	private static final String CODE_IS_VALID = "0";
	private static final String CODE_IS_NOT_VALID = "1";
	private static final String OTHER_ERROR = "2";
	private static final String IO_ERROR_MESSAGE = "A problem occured when trying to read from the file.";
	private static final String NON_SJAVA_FILE_MESSAGE = "Warning: Running on a non s-java file";
	
	private final File sjavaFile;
	
	Manager(String path) {
		this.sjavaFile = new File(path);
	}
	void verify() {
		// create parser object
		Parser parser = new Parser(this.sjavaFile);
		// call parse on file
		try {
			boolean validCode = parser.parseFile();
			if(!this.sjavaFile.getName().endsWith(".sjava")) {
				System.err.println(NON_SJAVA_FILE_MESSAGE);
			}
			if(validCode) {
				System.out.println(CODE_IS_VALID);
			} else {
				String error = parser.getErrorMessage();
				System.err.println(error);
				System.out.println(CODE_IS_NOT_VALID);
			}
		} catch (IOException e) {
			System.err.println(IO_ERROR_MESSAGE);
			System.out.println(OTHER_ERROR);
		}
		
	}
}
