package oop.ex6.main;

/**
 * Main sjava verifier.
 *
 */
public class Sjavac {

	private static final String ARGUMENT_MISSING = "Error: Missing Argument file path";
	private static final int FILE_PATH_INDEX = 0;
	
	/**
	 * main gets args with file path and using manager object it verifies it.
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length > 0) {
			// get file path from args
			String filePath = args[FILE_PATH_INDEX];
			// create new manager object to handle verifying
			Manager manager = new Manager(filePath);
			manager.verify();
		} else {
			// if there's a missing argument, print error to err
			System.err.println(ARGUMENT_MISSING);
		}
	}
	
}
