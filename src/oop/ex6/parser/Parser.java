package oop.ex6.parser;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.HashMap;
import java.util.List;
import oop.ex6.sjava.CompileErrorException;
import oop.ex6.sjava.GlobalScope;
import oop.ex6.sjava.Method;
import oop.ex6.sjava.Scope;
import oop.ex6.sjava.expression.Expression;
import oop.ex6.sjava.expression.ExpressionFactory;

/**
 * A class that parses a sjava file, and checks if its code will pass sjava compilation or not.
 *
 */
public class Parser {
	
	private final static String UNCLOSED_SCOPE_ERROR_MSG = "The file ends with an unclosed scope";
	private final static String ERROR_MESSAGE_HEADER = "A problem occurred in line ";
	private final static String LINE_END_ERROR_MSG = ":\n";
	private final static int GLOBAL_DEPTH = 0;
	private String errorMessage;
	private File sjavaFile;
	private LineNumberReader reader;
	
	/**
	 * Constructor- creates a new Parser object.
	 * @param sjavaFile The file that the parser will parse.
	 */
	public Parser(File sjavaFile) {
		this.sjavaFile = sjavaFile;
	}
	
	/**
	 * Parses an sjava file, and returns it's validity.
	 * @return is true iff the code in the file is valid.
	 */
	public boolean parseFile() throws IOException {
		GlobalScope globalScope = new GlobalScope(); // create the global scope
		this.reader = new LineNumberReader(new FileReader(this.sjavaFile)); // create new line number reader
		// Use parseHelper to convert file contents with expression and methods
		boolean validStructure = parseHelper(globalScope);
		
		if(validStructure) {
			HashMap<String, Method> allMethods = globalScope.getMethods(); // holds all methods in file
			// save the global variables state
			globalScope.saveGlobalVariablesState();
			boolean validMethod;
			for(Method method : allMethods.values()) { // for each method - check validity
				validMethod = this.executeMethod(method);
				// make the global variable return to it's initial state, so the next method will get a fresh
				// globals again
				globalScope.resetSavedGlobalState();
				if(!validMethod) { // method is invalid
					validStructure = false;
					break;
				}
			}
		}
		return validStructure;
	}
	
	/**
	 * Getter for errorMessage member.
	 * @return The error message. If the file is valid, or it did not parse it yet- will return null.
	 */
	public String getErrorMessage() {
		return this.errorMessage;
	}

	/*
	 * Parses the whole file, and converts all the text to sjava expressions. For the expressions/lines that
	 * are in the global scope, it will execute them, so that at the end- globalScope will be populated with
	 * the global methods and global variables.
	 * @param GlobalScope The global scope of the sjava file.
	 * @return true iff the global structure of the code is valid.
	 * @throws IOException if there is a problem reading from the file.
	 */
	private boolean parseHelper(GlobalScope globalScope) throws IOException {
		boolean isValid = true; // file is innocent until proven guilty
		String line = this.reader.readLine(); // read first line
		List<Expression> expressions;
		int depth = 0;
		while((line != null) && (isValid)){ // while the file didn't end, and is currently valid
			try {
				expressions = ExpressionFactory.createExpressions(line); // get list of expression from line
				if(depth == GLOBAL_DEPTH) {
					// if current scope is global, execute each expression in line.
					for(Expression expression : expressions) {
						expression.executeExpression(globalScope);
						if(expression.isMethodDecleration()) {
							// if new method was declared, update it's deceleration line in it.
							Method method = globalScope.getNewestMethod();
							method.setDeclarationLineNumber(this.reader.getLineNumber());
						}
					}
				} else {
					// if scope is not global, don't execute each expression but rather just add it to
					// current method's body
					Method method = globalScope.getNewestMethod();
					method.addExrepssions(expressions);
				}
				// for every expression in this line, increase or decrease depth as needed
				for(Expression expression : expressions) {
					if(expression.isScopeStart()) {
						depth++;
					} else if (expression.isScopeEnd()) {
						depth--;
					}
				} 
			} catch (CompileErrorException|UnsupportedOperationException e) {
				this.assignErrorMessage(e.getMessage(), this.reader.getLineNumber());
				isValid = false;
			}
			line = this.reader.readLine();
		}
		if((isValid) && (depth > 0)) {
			// if run ends with depth greater than zero, there is a scope left open.
			this.assignErrorMessage(UNCLOSED_SCOPE_ERROR_MSG, this.reader.getLineNumber());
			isValid = false;
		}
		return isValid;
	}
	
	/*
	 * A method that gets a string that holds the information about the error that occurred, and updates
	 * the error message accordingly.
	 * @param errorMessage The information regarding the error that occurred.
	 * @param errorLine The line number in which the error occurred.
	 */
	private void assignErrorMessage(String errorMessage, int errorLine) {
		this.errorMessage = ERROR_MESSAGE_HEADER + errorLine + LINE_END_ERROR_MSG + errorMessage;
	}
	
	/*
	 * Single method verifier. Executes all the expressions that are in the method body, to verify the
	 * the code is valid.
	 * @param method The method scope that is being parsed.
	 * @return true iff the method code is legal.
	 */
	private boolean executeMethod(Method method) {
		boolean isValid = true; // code is valid unless error found
		List<List<Expression>> methodBody = method.getMethodBody();
		int methodStartLine = method.getDeclarationLineNumber(); // Line number of method declaration
		int currentLineNumber = 1;
		Scope currentScope = method;
		try {
			for(List<Expression> lineExpressions : methodBody) {
				for(Expression expression : lineExpressions) {
					// for each expression in method body, do execute.
					expression.executeExpression(currentScope);
					if(expression.isScopeStart()) { // If this expression starts a scope - create it.
						currentScope = currentScope.getInnerScope();
					} else if(expression.isScopeEnd()) { // If this expression ends a scope - close it.
						currentScope = currentScope.getOuterScope();
					}
				}
				currentLineNumber++;
			}
		} catch (CompileErrorException|UnsupportedOperationException e) {
			// log the error message and turn isValid to false.
			this.assignErrorMessage(e.getMessage(), methodStartLine + currentLineNumber);
			isValid = false;
		}
		return isValid; 
	}
	
}
