package oop.ex6.sjava;

/**
 * This is a super exception class for any compile error of sjava files.
 *
 */
public class CompileErrorException extends Exception {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor
	 * @param message The information about the reason that the exception was thrown.
	 */
	public CompileErrorException(String message){
		super(message);
	}
}
