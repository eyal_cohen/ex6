package oop.ex6.sjava;

import java.util.HashMap;

/**
 * Represents a global scope of a sjava program.
 *
 */
public class GlobalScope extends Scope {
	
	private final static String Exception_MSG_1 = "Cannot use 'return' statement in global scope.";
	
	private Method newestMethod;
	private HashMap<String, Variable> variablesCopy;
	private boolean savedGlobalVariables;
	
	/**
	 * Constructor
	 */
	public GlobalScope() {
		super();
		variablesCopy = new HashMap<String, Variable>();
		this.savedGlobalVariables = false;
	}
	
	/**
	 * getter for methods.
	 * @return HashMap of methods
	 */
	public HashMap<String, Method> getMethods() {
		return this.methods;
	}
	
	/**
	 * getter for newest method added to the scope.
	 * @return method The newest method added.
	 */
	public Method getNewestMethod() {
		return this.newestMethod;
	}
	
	/**
	 * A method, that makes the global scope remember its variables current state- meaning, when the method
	 * 'resetSavedGlobalState' is called- the global scope's variables will return to the state it was when
	 * this method was called (all changes that happened since will be erased).
	 */
	public void saveGlobalVariablesState() {
		copyVariables(this.variables, this.variablesCopy);
		this.savedGlobalVariables = true;
	}
	
	/**
	 * A method, that resets the global variables to the state they were in the last time that
	 * 'saveGlobalVariablesState' was called.
	 * @throws IllegalStateException if the method was called without 'saveGlobalVariablesState' being
	 * called before.
	 */
	public void resetSavedGlobalState() {
		if(this.savedGlobalVariables == false) {
			throw new IllegalStateException();
		}
		copyVariables(this.variablesCopy, this.variables);
	}
	
	@Override
	public void addMethod(Method method) throws MethodAlreadyExistsException {
		super.addMethod(method);
		this.newestMethod = method;
	}
	
	@Override
	public void setInnerScope(Scope scope) {
		if(scope != null) { // the global scope doesn't have a specific inner scope, besides the methods
			throw new UnsupportedOperationException();
		}
	}
	
	@Override
	public void setCanEnd(boolean canEnd) throws UnsupportedOperationException{
		throw new UnsupportedOperationException(Exception_MSG_1);
	}
	
	/*
	 * A helper method, that copies all the variables from one hash map to another (creates new copies,
	 * doesn't pass the reference).
	 * @param source The hash map that holds the variables that will be copied.
	 * @param target The hash map that will receive the copies of the variables.
	 */
	private static void copyVariables(HashMap<String, Variable> source, HashMap<String, Variable> target) {
		target.clear();
		for(Variable var : source.values()) {
			Variable newVar = new Variable(var);
			target.put(newVar.getName(), newVar);
		}
	}
}
