package oop.ex6.sjava;

/**
 * A scope object, that represents an if or while block.
 *
 */
public class IfAndWhileScope extends Scope {

	private final static String EXCEPTION_MSG_1 = "Can't declare a new method in an if or while statement.";
	
	/**
	 * Constructor
	 * @param outerScope - the scope in which this scope is declared.
	 */
	public IfAndWhileScope(Scope outerScope) {
		super(outerScope);
	}
	
	@Override
	public void addMethod(Method method) throws MethodAlreadyExistsException {
		// declaring a method is not supported for this scope
		throw new UnsupportedOperationException(EXCEPTION_MSG_1);
	}
	
}
