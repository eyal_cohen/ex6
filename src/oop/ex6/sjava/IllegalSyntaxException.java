package oop.ex6.sjava;

/**
 * Thrown for any wrong syntax in the sjava file.
 *
 */
public class IllegalSyntaxException extends CompileErrorException {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor
	 * @param message The information about the reason that the exception was thrown.
	 */
	public IllegalSyntaxException(String message){
		super(message);
	}
}
