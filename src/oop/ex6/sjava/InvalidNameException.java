package oop.ex6.sjava;

/**
 * Thrown for declaring a variable or method with an illegal name.
 *
 */
public class InvalidNameException extends CompileErrorException {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * @param message The information about the reason that the exception was thrown.
	 */
	public InvalidNameException(String message) {
		super(message);
	}

}
