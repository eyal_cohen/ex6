package oop.ex6.sjava;

import java.util.ArrayList;
import java.util.List;

import oop.ex6.sjava.expression.Expression;
import oop.ex6.sjava.types.Type;

/**
 * Represents a method scope in a sjava program.
 *
 */
public class Method extends Scope{

	private final static String EXCEPTION_MSG_1 = "Cannot declare a new method inside an existing one.";
	
	private final Type returnType; // method's return type
	private final String name; // name of method
	private List<List<Expression>> methodBody; // The method body after conversion to expressions
	private List<Type> parametersType; // list of variables in method's header
	private final int numberOfParameters; // number of parameters that the method gets in its header
	
	/**
	 * Creates a new method scope object.
	 * @param returnType The return type of the method (eg void, etc).
	 * @param name The name of the method.
	 * @param numberOfParameters The amount of parameters that the method receives.
	 * @param outerScope The scope in which this method is declared.
	 */
	public Method(Type returnType, String name, int numberOfParameters, Scope outerScope) {
		super(outerScope);
		this.returnType = returnType;
		this.name = name;
		this.methodBody = new ArrayList<List<Expression>>();
		this.parametersType = new ArrayList<Type>();
		this.canEnd = false;
		this.numberOfParameters = numberOfParameters;
	}
	
	@Override
	public void addVariable(Variable var)
							throws VariableAlreadyExistsException, UnassignedFinalVariableException {
		if(parametersType.size() < this.numberOfParameters) {
			// meaning that var belongs to the parameters in the methods header- add var to parameters list
			this.parametersType.add(var.getType());
			// initialize it
			var.Initialize(); // because it is received as a parameter
		}
		super.addVariable(var);
		this.setCanEnd(false); // can't end after adding a var- needs return statement
	}
	
	@Override
	public Variable findVariable(String varName) {
		this.setCanEnd(false); // can't end after finding a var- needs return statement
		return super.findVariable(varName);
	}
	
	/**
	 * getter for methodBody list.
	 * @return the method body- all the expressions, in each line in the method body.
	 * (each line being a list of its own)
	 */
	public List<List<Expression>> getMethodBody() {
		return this.methodBody;
	}
	
	/**
	 * Add's expressions that were created by one line to the method body.
	 * @param expression The expressions in the line that are in the method.
	 */
	public void addExrepssions(List<Expression> expressions) {
		this.methodBody.add(expressions);
	}
	
	/*
	 * Returns the name of the method.
	 * 
	 * @return The name of the method.
	 */
	String getName() {
		return this.name;
	}
	
	/*
	 * Returns the method's return type.
	 * 
	 * @return The return type of the method.
	 */
	Type getReturnType() {
		return this.returnType;
	}
	
	/**
	 * Returns the parameters types of the method, that appear in the methods declaration line.
	 * 
	 * @return The parameters types of the method, that appear in the methods declaration line.
	 */
	public List<Type> getParameters() {
		return this.parametersType;
	}
	
	@Override
	public void addMethod(Method method) throws MethodAlreadyExistsException {
		// declaring a method inside another method is not supported
		throw new UnsupportedOperationException(EXCEPTION_MSG_1);
	}
}
