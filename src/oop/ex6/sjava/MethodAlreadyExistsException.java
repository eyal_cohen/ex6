package oop.ex6.sjava;

/**
 * Thrown when trying to declare a method that already exists.
 *
 */
public class MethodAlreadyExistsException extends CompileErrorException {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor
	 * @param message The information about the reason that the exception was thrown.
	 */
	public MethodAlreadyExistsException(String message) {
		super(message);
	}

}
