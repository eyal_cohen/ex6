package oop.ex6.sjava;

import java.util.HashMap;

/**
 * An abstract class, that represents objects that are a scope in a sjava program.
 */
public abstract class Scope {
	
	private static final String EXCEPTION_MSG_1 = "Cannot declare a final variable without initialization.";
	private static final String EXCEPTION_MSG_2 = "Variable '";
	private static final String EXCEPTION_MSG_3 = "' Already Exists";
	private static final String EXCEPTION_MSG_4 = "Method with name: ";
	private static final String EXCEPTION_MSG_5 = "Already exists";
	
	private static final int NO_START_LINE_VALUE = -1;
	private final static String DEFAULT_SCOPE_NAME = "scope";
	HashMap<String, Variable> variables;
	HashMap<String, Method> methods;
	Scope innerScope, outerScope;
	private int declarationLineNumber;
	boolean canEnd;
	
	/**
	 * Default constructor.
	 */
	public Scope() {
		this.variables = new HashMap<String, Variable>();
		this.declarationLineNumber = NO_START_LINE_VALUE;
		this.methods = new HashMap<String, Method>();
		this.outerScope = null;
		this.innerScope = null;
		this.canEnd = true;
	}
	
	/*
	 * Constructor. Gets the outer scope, in which this scope was declared.
	 * @param outerScope The scope in which this scope was declared.
	 */
	Scope(Scope outerScope){
		this();
		this.outerScope = outerScope;
	}

	/**
	 * Add's a variable to the scope.
	 * @param var The variable that is being added.
	 * @throws VariableAlreadyExistsException if there is already a variable with the same name in the scope.
	 * @throws UnassignedFinalVariableException if an uninitialized final variable is being added.
	 */
	public void addVariable(Variable var)
						throws VariableAlreadyExistsException, UnassignedFinalVariableException {
		if((var.isFinal()) && (!var.isInitialized())) { // a final variable is added without being initialized
			throw new UnassignedFinalVariableException(EXCEPTION_MSG_1);
		}
		if(!this.variables.containsKey(var.getName())) {
			this.variables.put(var.getName(), var);
		} else {
			// variable with the same name exists in this scope
			throw new VariableAlreadyExistsException(EXCEPTION_MSG_2 + var.getName() + EXCEPTION_MSG_3);
		}
	}
	
	/**
	 * Searches for a variable. It searches from this scope to the global scope.
	 * @param name The name of the variable that is being searched for.
	 * @return The variable if it was found, null otherwise.
	 */
	public Variable findVariable(String name) {
		Variable var = variables.get(name);
		if(var == null) {
			if(this.outerScope != null) {
				var = this.outerScope.findVariable(name);
			}
		}
		return var;
	}
	
	/**
	 * Sets an inner scope to the current one.
	 * @param Scope the scope that is the inner scope.
	 * @throws UnsupportedOperationException if it is a specific scope type that does not support this
	 * methods action (eg GlobalScope).
	 */
	public void setInnerScope(Scope scope) {
		this.innerScope = scope;
	}
	
	/**
	 * add's a Method to the list of methods that this scope contains.
	 * @param method The method that is being added.
	 * @throws MethodAlreadyExistsException if there is already a method with the same name.
	 * @throws UnsupportedOperationException if it is a specific scope that does not support adding a
	 * new method to them (eg if/while scope, method scope).
	 */
	public void addMethod(Method method) throws MethodAlreadyExistsException {
		if(this.methods.containsKey(method.getName())) {
			throw new MethodAlreadyExistsException(EXCEPTION_MSG_4 + method.getName() + EXCEPTION_MSG_5);
		}
		this.methods.put(method.getName(), method);
	}
	
	/**
	 * sets the line number of this scope's declaration, in the sjava program.
	 * @param num The line declaration number.
	 */
	public void setDeclarationLineNumber(int num) {
		this.declarationLineNumber = num;
	}
	
	/**
	 * Returns the line number in which the declaration of the method appeared.
	 * 
	 * @return the line number in which the declaration of the method appeared. If the line number was not
	 * set- will return -1.
	 */
	public int getDeclarationLineNumber() {
		return this.declarationLineNumber;
	}
	
	/*
	 * return scope's name
	 */
	String getName() {
		return DEFAULT_SCOPE_NAME;
	}
	
	/**
	 * getter for innerScope.
	 * @return the inner scope of this scope.If none is set, will return null.
	 */
	public Scope getInnerScope() {
		return this.innerScope;
	}
	
	/**
	 * getter for outerScope.
	 * @return the outer scope of this scope. If none is set, will return null.
	 */
	public Scope getOuterScope() {
		return this.outerScope;
	}
	
	/**
	 * Can the scope in its current state end?
	 * @return true iff the scope can now end (close).
	 */
	public boolean canEnd() {
		return this.canEnd;
	}
	
	/**
	 * setter for canEnd
	 * @param canEnd if the scope will now be able to end or not (true/false).
	 * @throws UnsupportedOperationException if it is a specific scope type that does not support this
	 * setters action (eg GlobalScope).
	 */
	public void setCanEnd(boolean canEnd) {
		this.canEnd = canEnd;
	}

	/**
	 * Searches for a method by it's name.
	 * @param name The name of the method that is searched.
	 * @return The desired method if found, null otherwise.
	 */
	public Method findMethod(String name) {
		Method method = methods.get(name);
		if(method == null) {
			if(this.outerScope != null) {
				method = this.outerScope.findMethod(name); // search in outer scopes for the method
			}
		}
		return method;
	}

	
}
