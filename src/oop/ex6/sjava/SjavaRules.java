package oop.ex6.sjava;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class holds general rules for sjava files.
 *
 */
public class SjavaRules {
	
	// reserved words
	public static final String FALSE = "false";
	public static final String TRUE = "true";
	
	private final static String VARIABLE_PATTERN = "(_[A-Za-z_\\d]+)|[A-Za-z][A-Za-z_\\d]*";
	private final static String METHOD_PATTERN = "[A-Za-z][A-Za-z_\\d]*";
	private static Pattern variablePattern = Pattern.compile(VARIABLE_PATTERN);
	private static Pattern methodPattern = Pattern.compile(METHOD_PATTERN);
	
	/**
	 * Check if a given name is legal as a variable name. 
	 * @param name The given name.
	 * @return true iff it's a legal name.
	 */
	public static boolean isLegalVariableName(String name) {
		return isNameLegal(name, variablePattern);
	}
	
	/**
	 * Check if a given name is legal as a method name. 
	 * @param name The given name.
	 * @return true iff it's a legal name.
	 */
	public static boolean isLegalMethodName(String name) {
		return isNameLegal(name, methodPattern);
	}
	
	/*
	 * A helper for methods checking if a name is legal.
	 * @param name The name of the object
	 * @param pattern The pattern for a legal name for the object.
	 * @return true iff its a legal name.
	 */
	private static boolean isNameLegal(String name, Pattern pattern) {
		Matcher matcher = pattern.matcher(name);
		if(matcher.matches()) {
			return true;
		}
		return false;
	}
	
}