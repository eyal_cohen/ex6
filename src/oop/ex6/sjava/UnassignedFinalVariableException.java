package oop.ex6.sjava;

/**
 * Thrown when a final variable is declared but not initialized in the same line.
 *
 */
public class UnassignedFinalVariableException extends CompileErrorException {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor
	 * @param message The information about the reason that the exception was thrown.
	 */
	public UnassignedFinalVariableException(String message) {
		super(message);
	}
}
