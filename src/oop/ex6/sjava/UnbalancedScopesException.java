package oop.ex6.sjava;

/**
 * Thrown when a scope is closed while there was no open one.
 *
 */
public class UnbalancedScopesException extends CompileErrorException {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor
	 * @param message The information about the reason that the exception was thrown.
	 */
	public UnbalancedScopesException(String message) {
		super(message);
	}
}
