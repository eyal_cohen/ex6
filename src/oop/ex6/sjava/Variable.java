package oop.ex6.sjava;

import oop.ex6.sjava.types.Type;

/**
 * A class that represents a variable in a sjava program.
 *
 */
public class Variable {
	
	private final static String EXCEPTION_MSG_1 = "Invalid name for variable: ";
	
	private final Type type; // variable type
	private final String name; // variable name
	private final boolean isFinal; // is this a final variable?
	private boolean isInitialized; // is this variable initialized?
	
	/**
	 * Creates a new Variable object.
	 * @param type The type of the variable.
	 * @param name The name of the variable.
	 * @param isFinal Is this a final variable?
	 * @param isInitialized - is this variable initialized?
	 * @throws InvalidNameException If the name of the variable is not a legal name.
	 */
	public Variable(Type type, String name, boolean isFinal, boolean isInitialized) throws InvalidNameException {
		this.type = type;
		if(SjavaRules.isLegalVariableName(name)) {
			this.name = name;
		} else {
			throw new InvalidNameException(EXCEPTION_MSG_1 + name);
		}
		this.isFinal = isFinal;
		this.isInitialized = isInitialized;
	}
	
	/*
	 * A copy constructor- that gets an existing variable, and creates a copy of it.
	 * 
	 * @param varToCopy The variable that is being copied.
	 */
	Variable(Variable varToCopy) {
		// type, name and isFinal are all final data members, and therefore cannot change- meaning there
		// is no need to create a copy. isInitialized is a primitive, so no need to copy reference.
		this.type = varToCopy.type;
		this.name = varToCopy.name;
		this.isFinal = varToCopy.isFinal;
		this.isInitialized = varToCopy.isInitialized;
	}
	
	/**
	 * Getter for the type.
	 * @return The Type of the variable.
	 */
	public Type getType() {
		return this.type;
	}
	
	/**
	 * Getter for the name.
	 * @return The name of the variable.
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Checks if the variable is final.
	 * @return true iff the variable is final
	 */
	public boolean isFinal() {
		return this.isFinal;
	}
	
	/**
	 * Checks if the variable is initialized.
	 * @return true iff the variable is initialized.
	 */
	public boolean isInitialized() {
		return this.isInitialized;
	}
	
	/**
	 * Sets the variable to be initialized.
	 */
	public void Initialize() {
		this.isInitialized = true;
	}

}