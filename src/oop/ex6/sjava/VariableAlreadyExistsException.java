package oop.ex6.sjava;

/**
 * Thrown when two variables are declared with the same name, in the same scope.
 *
 */
public class VariableAlreadyExistsException extends CompileErrorException {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * @param message The information about the reason that the exception was thrown.
	 */
	public VariableAlreadyExistsException(String message) {
		super(message);
	}

}
