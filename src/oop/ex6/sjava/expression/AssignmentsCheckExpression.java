package oop.ex6.sjava.expression;

import oop.ex6.sjava.Scope;
import oop.ex6.sjava.SjavaRules;
import oop.ex6.sjava.Variable;
import oop.ex6.sjava.types.Type;
import oop.ex6.sjava.types.TypeDoesntExistException;
import oop.ex6.sjava.types.TypeFactory;

/*
 * An abstract class for expressions that need to make sure that an assignment to a variable is legal.
 *
 */
abstract class AssignmentsCheckExpression extends Expression {

	private static final String EXCEPTION_MSG_1 = "Type mismatch: Can't initialize ";
	private static final String EXCEPTION_MSG_2 = " with value: ";
	private static final String EXCEPTION_MSG_3 = "Can't Initialize ";
	private static final String EXCEPTION_MSG_4 = " with a boolean";
	private static final String EXCEPTION_MSG_5 = " Can't be resolved to a variable";
	private static final String EXCEPTION_MSG_6 = "The variable ";
	private static final String EXCEPTION_MSG_7 = " has not been initialized";
	private static final String EXCEPTION_MSG_8 = "Type mismatch: Can't initialize ";
	private static final String EXCEPTION_MSG_9 = " with ";
	private static final String PERIOD = ".";
	
	/*
	 * A helper method, that checks if a given value can be assigned into a
	 * specific type. The value can be either a direct value (for example: a
	 * number - 2, a string- "something", etc), or an existing variable's name.
	 * 
	 * @param scope The scope in which the assignment is taking place.
	 * 
	 * @param val The value that is being assigned.
	 * 
	 * @param varType The type which is being assigned into.
	 * 
	 * @throws TypeMismatchException if the value is not of a type that can be
	 * assigned into a variable of type 'varType'.
	 * 
	 * @throws VariableDoesntExistException if 'val' is a variables name, but
	 * such a variable doesn't exist.
	 */
	void assignmentCheck(Scope scope, String val, Type varType)
			throws TypeMismatchException, VariableDoesntExistException,
			TypeDoesntExistException, UninitializedVariableAssignmentException {
		if (varType.canAsign(val)) {
			return;
		}
		if (!SjavaRules.isLegalVariableName(val)) {
			// Value cannot be assigned to var and value is not a legal variable
			// name
			throw new TypeMismatchException(EXCEPTION_MSG_1 + varType.getStringRepresentation()
					+ EXCEPTION_MSG_2 + val + PERIOD);
		}
		if (val.equals(SjavaRules.TRUE) || val.equals(SjavaRules.FALSE)) {
			Type booleanType = TypeFactory
					.createVariableType(TypeFactory.BOOLEAN);
			if (!varType.canAsign(booleanType)) {
				throw new TypeMismatchException(EXCEPTION_MSG_3
						+ varType.getStringRepresentation() + EXCEPTION_MSG_4 + PERIOD);
			}
		}
		// Value cannot be assigned to var and value is a legal variable name
		Variable var2 = scope.findVariable(val);
		if (var2 == null) {
			throw new VariableDoesntExistException(val + EXCEPTION_MSG_5);
		} else { // value is a name of existing variable (var2).
			if (!var2.isInitialized()) {
				throw new UninitializedVariableAssignmentException( EXCEPTION_MSG_6 + var2.getName()
						+ EXCEPTION_MSG_7 + PERIOD);
			}
			if (varType.canAsign(var2.getType())) { // var2 can be assigned to var
				return;
			} else {
				throw new TypeMismatchException(EXCEPTION_MSG_8 + varType.getStringRepresentation() 
						+ EXCEPTION_MSG_9 + var2.getType().getStringRepresentation() + PERIOD);
			}
		}
	}

}