package oop.ex6.sjava.expression;

import oop.ex6.sjava.CompileErrorException;

/**
 * Thrown when a scope is tried to be close (i.e. with "}") but it's illegal to close it (for
 * example if it's a method and there wasn't a return statement right before the end of it).
 *
 */
public class CantEndScopeException extends CompileErrorException {

	private static final long serialVersionUID = 1L;

	/**
	 * constructor
	 * @param message The information about the reason that the exception was thrown.
	 */
	public CantEndScopeException(String message) {
		super(message);
	}

}
