package oop.ex6.sjava.expression;

import oop.ex6.sjava.CompileErrorException;
import oop.ex6.sjava.Scope;

/*
 * An expression for a comment in a sjava program.
 */
class CommentExpression extends Expression {

	@Override
	public void executeExpression(Scope scope) throws CompileErrorException {
		// comment line- do Nothing
	}

}
