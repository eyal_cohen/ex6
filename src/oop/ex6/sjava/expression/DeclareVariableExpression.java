package oop.ex6.sjava.expression;

import oop.ex6.sjava.CompileErrorException;
import oop.ex6.sjava.Scope;
import oop.ex6.sjava.Variable;
import oop.ex6.sjava.types.Type;
import oop.ex6.sjava.types.TypeFactory;

/*
 * An expression for declaring a new variable.
 *
 */
class DeclareVariableExpression extends AssignmentsCheckExpression {
	private final String typeName; // name of the type for the new variable
	private final String name; // name of new variable
	private final boolean isFinal; // is the new variable final?
	private final String value;// the value that was assigned at declaration time
	
	/*
	 * Constructor
	 * @param type The variables type.
	 * @param name The variables name.
	 * @param isFinal true/false for final/not final.
	 * @param value The value that the variable was assigned at declaration. (null if wasn't)
	 */
	DeclareVariableExpression(String type, String name, boolean isFinal, String value) {
		this.typeName = type;
		this.name = name;
		this.isFinal = isFinal;
		this.value = value; // if the variable was not initialized at declaration time- it will be null
	}
	
	@Override
	public void executeExpression(Scope scope) throws CompileErrorException {
		Type type = TypeFactory.createVariableType(this.typeName);
		// create new variable
		Variable var = new Variable(type, this.name, this.isFinal, false);
		if(value != null) { // if it was assigned a value, check it is valid
			assignmentCheck(scope, this.value, type);
			var.Initialize();
		}
		// add variable to memory
		scope.addVariable(var);
	}
	
}
