package oop.ex6.sjava.expression;

import oop.ex6.sjava.CompileErrorException;
import oop.ex6.sjava.Scope;

/*
 * An expression for an empty line a a sjava program.
 *
 */
class EmptyLineExpression extends Expression {
	
	@Override
	public void executeExpression(Scope scope)
			throws CompileErrorException {
		// empty line- doesn't do anything
	}

}
