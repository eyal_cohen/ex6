package oop.ex6.sjava.expression;

import oop.ex6.sjava.CompileErrorException;
import oop.ex6.sjava.Scope;

/**
 * Expression class. Every bit of code in the sjava code is converted to an expression.
 * Expressions could be variable declarations, return statements or just an empty line, among many others.
 *
 */
public abstract class Expression {
	
	/**
	 * Does this expression start a scope?
	 * @return true iff the expression opens a new scope.
	 */
	public boolean isScopeStart() {
		return false;
	}
	
	/**
	 * Does this expression close a scope?
	 * @return true iff the expression closes a scope.
	 */
	public boolean isScopeEnd() {
		return false;
	}
	
	/**
	 * Is this expression declaring a new method?
	 * @return true iff it declares a new method.
	 */
	public boolean isMethodDecleration() {
		return false;
	}
	
	/**
	 * Executes an expression, on the scope it receives. The real work is done in this method.
	 * @param scope The scope on which the expression will do its actions on.
	 * @throws CompileErrorException If a problem occurs while trying to execute.
	 */
	abstract public void executeExpression(Scope scope) throws CompileErrorException;
	
}