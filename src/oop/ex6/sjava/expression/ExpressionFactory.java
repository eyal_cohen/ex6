package oop.ex6.sjava.expression;

import oop.ex6.sjava.expression.Expression;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oop.ex6.sjava.CompileErrorException;
import oop.ex6.sjava.IllegalSyntaxException;


public class ExpressionFactory {
	
	private final static String EXCEPTION_MSG_1 = "There is an extra comma in the variable declaration line.";
	private final static String EXCEPTION_MSG_2 = "There is a '=' sign without 2 values on both sides.";
	private final static String EXCEPTION_MSG_3 = "Bad syntax in variable numer ";
	private final static String EXCEPTION_MSG_4 = " declaration.";
	private final static String EXCEPTION_MSG_5 = "missing a variable name, for variable number ";
	private final static String EXCEPTION_MSG_6 = "missing a variable value, for variable number ";
	private final static String EXCEPTION_MSG_7 = "There is an extra comma at the edge of the parameters that"
			+ " the method receives.";
	private final static String EXCEPTION_MSG_8 = "Syntax error";
	private final static String EXCEPTION_MSG_9 = "A parameter cannot get a value during a method"
			+ " declaration.";
	private final static String EXCEPTION_MSG_10 = "Illegal syntax in method parameters declaration.";
	private final static String EXCEPTION_MSG_11 = "Missing a condition at the end, before the closing "
			+ " bracket.";
	private final static String EXCEPTION_MSG_12 = "Missing a condition in the brackets.";
	private final static String EXCEPTION_MSG_13 = "There is no value being assigned into the variable.";
	private final static String EXCEPTION_MSG_14 = "There is an extra comma at the edge of the parameters"
			+ " that are sent to the method.";

	private static final String COMMENT_REGEX = "//";
	private static final String VAR_PREFIX = "final";
	private static final String VAR_TYPE = "([A-Za-z]+)";
	private static final String VAR_NAME = "\\w+";
	private static final String VAR_SEPARATOR = ",";
	private static final String VAR_ASSIGNMENT = "=";
	private static final String END_LINE = ";";
	private static final String EMPTY_STRING = "";
	private static final String VAR_DECLARATION_LINE = "(\\s*" + VAR_PREFIX + "\\s)?\\s*" + VAR_TYPE
													+ "\\s+" + "(" +VAR_NAME + ".*)" + END_LINE + "\\s*";
	private static final String VAR_INITIALIZATION_LINE = "\\s*(" + VAR_NAME + ")\\s*" + VAR_ASSIGNMENT
																	+ "\\s*" + "(.*)" + END_LINE + "\\s*";
	private static final String METHOD_TYPE = "[A-Za-z]+";
	private static final String METHOD_NAME = "\\w+";
	private static final String METHOD_PARAMETERS_START = "\\(";
	private static final String METHOD_PARAMETERS_END = "\\)";
	private static final String OPEN_SCOPE = "\\{";
	private static final String CLOSE_SCOPE = "\\}";
	private static final String METHOD_DECLARATION_LINE = "\\s*(" + METHOD_TYPE + ")\\s+(" + METHOD_NAME
		+ ")\\s*" + METHOD_PARAMETERS_START + "(.*)" + METHOD_PARAMETERS_END + "\\s*" + OPEN_SCOPE + "\\s*";
	
	private static final String METHOD_CALL_LINE = "\\s*(" + METHOD_NAME + ")\\s*" + METHOD_PARAMETERS_START
											+ "(.*)" + METHOD_PARAMETERS_END + "\\s*" + END_LINE + "\\s*";
	private static final String CLOSE_SCOPE_LINE = "\\s*" + CLOSE_SCOPE + "\\s*";
	private static final String EMPTY_LINE = "\\s*";
	private static final String IF_OR_WHILE_STATEMENT = "(?:if|while)";
	private static final String CONDITION_START = "\\(";
	private static final String CONDITION_END = "\\)";
	private static final String CONDITIONS_SEPARATOR_AND = "&&";
	private static final String CONDITIONS_SEPARATOR_OR = "||";
	private static final String CONDITIONS_SEPARATOR = "(?:&&|\\|\\|)";
	private static final String IF_OR_WHILE_LINE = "\\s*" + IF_OR_WHILE_STATEMENT + "\\s*" + CONDITION_START
													+ "(.*)" + CONDITION_END + "\\s*" + OPEN_SCOPE + "\\s*";
	private static final String METHOD_RETURN = "return";
	private static final String RETURN_STATEMENT = "\\s*" + METHOD_RETURN + "\\s*" + END_LINE + "\\s*";
	private static final String VAR_DECLARATION = "(.*?)(?:" + VAR_ASSIGNMENT + "(.*))?";
	
	
	private static final Pattern variablesDeclaration = Pattern.compile(VAR_DECLARATION_LINE);
	private static final Pattern variableInitialization = Pattern.compile(VAR_INITIALIZATION_LINE);
	private static final Pattern methodDeclaration = Pattern.compile(METHOD_DECLARATION_LINE);
	private static final Pattern methodCall = Pattern.compile(METHOD_CALL_LINE);
	private static final Pattern closeScopeDeclaration = Pattern.compile(CLOSE_SCOPE_LINE);
	private static final Pattern emptyLinePattern = Pattern.compile(EMPTY_LINE);
	private static final Pattern ifOrWhilePattern = Pattern.compile(IF_OR_WHILE_LINE);
	private static final Pattern variableDeclarationPattern = Pattern.compile(VAR_DECLARATION);
	private static final Pattern returnStatementPattern = Pattern.compile(RETURN_STATEMENT);
	
	/**
	 * Create expression according to a line
	 * @param line
	 * @return List<Expression> - list of expressions
	 * @throws CompileErrorException
	 */
	public static List<Expression> createExpressions(String line) throws CompileErrorException{
		
		List<Expression> expressionList = new ArrayList<Expression>();
		Matcher emptyLineMatcher = emptyLinePattern.matcher(line);
		Matcher closeScopeMatcher = closeScopeDeclaration.matcher(line);
		Matcher returnStatementMatcher = returnStatementPattern.matcher(line);
		Matcher ifOrWhileMatcher = ifOrWhilePattern.matcher(line);
		Matcher methodCallMatcher = methodCall.matcher(line);
		Matcher methodDeclarationMatcher = methodDeclaration.matcher(line);
		Matcher variableInitializationMatcher = variableInitialization.matcher(line);
		Matcher varDeclarationMatcher = variablesDeclaration.matcher(line);
		
		if(line.startsWith(COMMENT_REGEX)) { // A comment line
			expressionList.add(new CommentExpression());
		} else if(emptyLineMatcher.matches()){
			expressionList.add(new EmptyLineExpression());
		} else if(closeScopeMatcher.matches()){
			expressionList.add(new ScopeEndExpression());
		} else if(returnStatementMatcher.matches()){
			expressionList.add(new ReturnStatementExpression());
		} else if(ifOrWhileMatcher.matches()){
			addIfOrWhileExpression(expressionList, ifOrWhileMatcher);
		} else if(methodCallMatcher.matches()){
			addMethodCallExpression(expressionList, methodCallMatcher);
		} else if(methodDeclarationMatcher.matches()){
			addMethodDeclarationExpression(expressionList, methodDeclarationMatcher);
		} else if(variableInitializationMatcher.matches()){
			addVariableInitializationExpression(expressionList, variableInitializationMatcher);
		} else if(varDeclarationMatcher.matches()){ // a variables declaration line
			addVariablesExpressions(expressionList, varDeclarationMatcher);
		} else {
			throw new IllegalSyntaxException(EXCEPTION_MSG_8);
		}
		return expressionList;
	}
	
	/*
	 * A method that gets a line that declares new variables, and adds the appropriate expressions to the
	 * expressions list.
	 * 
	 * @param expressionList The list of expressions.
	 * @param varDeclarationMatcher the matcher that has the line that declared new variables.
	 * @throws CompileErrorException if there is something wrong with the line.
	 */
	private static void addVariablesExpressions(List<Expression> expressionList, 
								Matcher varDeclarationMatcher) throws IllegalSyntaxException {
		
		String finalPrefix = varDeclarationMatcher.group(1);
		String typeName = varDeclarationMatcher.group(2);
		String declarations = varDeclarationMatcher.group(3).trim();
		//checks if there are extra comma's at the beginning or at the end
		if (!checkEdges(declarations, VAR_SEPARATOR)) {
			throw new IllegalSyntaxException(EXCEPTION_MSG_1);
		}
		boolean isFinal = false;
		if(finalPrefix != null){
			// because the regex identified the line as a variable declaration, the
			//prefix will be either null or 'final'
			isFinal = true;
		}
		String[] allVariables = declarations.split(VAR_SEPARATOR);
		Matcher variableDeclarationMatcher;
		for(int i = 1; i < allVariables.length + 1; i++){ // going over all the declarations
			String variableDeclaration = allVariables[i-1].trim();
			if (!checkEdges(variableDeclaration, VAR_ASSIGNMENT)) {
				throw new IllegalSyntaxException(EXCEPTION_MSG_2);
			}
			variableDeclarationMatcher = variableDeclarationPattern.matcher(variableDeclaration);
			if(!variableDeclarationMatcher.matches()){
				throw new IllegalSyntaxException(EXCEPTION_MSG_3 + i + EXCEPTION_MSG_4);
			}
			String varName = variableDeclarationMatcher.group(1).trim();
			String value = variableDeclarationMatcher.group(2);
			if((varName.equals(EMPTY_STRING))) {
				throw new IllegalSyntaxException(EXCEPTION_MSG_5 + i);
			}
			if(value != null) {
				value = value.trim();
				if(value.equals(EMPTY_STRING)) {
					throw new IllegalSyntaxException(EXCEPTION_MSG_6 + i);
				}
			}
			expressionList.add(new DeclareVariableExpression(typeName, varName, isFinal, value));
		}
	}
	
	/*
	 * A method that gets a line that declares a new method, and adds the appropriate expressions to the
	 * expressions list.
	 * 
	 * @param expressionList The list of expressions.
	 * @param methodDeclarationMatcher the matcher that has the line that declared the method.
	 * @throws CompileErrorException if there is something wrong with the line.
	 */
	private static void addMethodDeclarationExpression(List<Expression> expressionList,
							Matcher methodDeclarationMatcher) throws CompileErrorException{
		
		String methodType = methodDeclarationMatcher.group(1);
		String methodName = methodDeclarationMatcher.group(2);
		String parameters = methodDeclarationMatcher.group(3).trim();
		
		if (!checkEdges(parameters, VAR_SEPARATOR)) {
			throw new IllegalSyntaxException(EXCEPTION_MSG_7);
		}
		List<Expression> parametersDeclarationList = new ArrayList<Expression>();
		
		if(!(parameters.equals(EMPTY_STRING))){
			String[] parametersArray = parameters.split(VAR_SEPARATOR);
			Matcher varDeclarationMatcher;
			for(String parameter:parametersArray){
				//each parameter declaration will be a expression of its own:
				if(parameter.contains(VAR_ASSIGNMENT)){
					throw new IllegalSyntaxException(EXCEPTION_MSG_9);
				}
				varDeclarationMatcher = variablesDeclaration.matcher(parameter + END_LINE);
				if(!(varDeclarationMatcher.matches())){
					throw new IllegalSyntaxException(EXCEPTION_MSG_10);
				}
				addVariablesExpressions(parametersDeclarationList, varDeclarationMatcher);
			}
		}
		expressionList.add(new MethodDeclarationExpression(methodName, methodType,
																			parametersDeclarationList));
	}
	
	/*
	 * A method that gets a line that is a start of a if/while scope, and adds the appropriate
	 * expression to the expressions list.
	 * 
	 * @param expressionList The list of expressions.
	 * @param ifOrWhileMatcher The matcher that has the line that is the if/while syntax.
	 * @throws CompileErrorExpression if there is something wrong with the syntax of the line.
	 */
	private static void addIfOrWhileExpression(List<Expression> expressionList,
			Matcher ifOrWhileMatcher) throws CompileErrorException{
		
		String allConditions = ifOrWhileMatcher.group(1).trim(); //all the conditions in the brackets
		
		if((allConditions.endsWith(CONDITIONS_SEPARATOR_AND)) ||
				(allConditions.endsWith(CONDITIONS_SEPARATOR_OR))){
			// ends with '&&' or '||' and doesn't have a condition after it
			throw new IllegalSyntaxException(EXCEPTION_MSG_11);
		}
		String[] allConditionsArray = allConditions.split(CONDITIONS_SEPARATOR);
		List<String> listOfConditions = new ArrayList<String>();
		for(String condition:allConditionsArray){ // each condition will be added to the list
			condition = condition.trim();
			if(condition.equals(EMPTY_STRING)){ // there is no condition
				throw new IllegalSyntaxException(EXCEPTION_MSG_12);
			}
			listOfConditions.add(condition);
		}
		expressionList.add(new IfAndWhileScopeExpression(listOfConditions));
	}
	
	/*
	 * A method that gets a line that puts a value in a variable, and adds the appropriate expression to
	 * the expressions list.
	 * 
	 * @param expressionList The list of expressions.
	 * @param variableInitializationMatcher The matcher that has the line that is the variable
	 * initialization syntax.
	 * @throws CompileErrorExpression if there is something wrong with the syntax of the line.
	 */
	private static void addVariableInitializationExpression(List<Expression> expressionList,
			Matcher variableInitializationMatcher) throws CompileErrorException{
		
		String variableName = variableInitializationMatcher.group(1);
		String value = variableInitializationMatcher.group(2).trim();
		
		if(value.equals(EMPTY_STRING)){
			throw new IllegalSyntaxException(EXCEPTION_MSG_13);
		}
		expressionList.add(new InitializeVariableExpression(variableName, value));
	}
	
	/*
	 * A method that gets a line that is a call to a method, and adds the appropriate expression to the
	 * expression list.
	 * 
	 * @param expressionList The list of expressions.
	 * @param methodCallMatcher The matcher that has the line that is the call to a method.
	 * @throws CompileErrorExpression if there is something wrong with the syntax of the line.
	 */
	private static void addMethodCallExpression(List<Expression> expressionList,
												Matcher methodCallMatcher) throws CompileErrorException {
		
		String methodName = methodCallMatcher.group(1);
		String parameters = methodCallMatcher.group(2).trim();
		
		if(!checkEdges(parameters, VAR_SEPARATOR)) {
			throw new IllegalSyntaxException(EXCEPTION_MSG_14);
		}
		List<String> parametersList = new ArrayList<String>();
		
		if(!(parameters.equals(EMPTY_STRING))){
			String[] parametersArray = parameters.split(VAR_SEPARATOR);
			
			for(String parameter:parametersArray){
				parametersList.add(parameter.trim());
			}
		}
		expressionList.add(new MethodCallExpression(methodName, parametersList));
	}
	
	/*
	 * A helper method, that gets a string that is the part of a line, and checks that it doesn't
	 * start or end with a specific undesired string.
	 * 
	 * @param strToCheck The string that is the part of the line being checked.
	 * @param strToSearch The string that shouldn't be at the edge of strToCheck.
	 * @throws IllegalSyntaxException if the string starts or ends with a strToSearch.
	 */
	private static boolean checkEdges(String strToCheck, String strToSearch) throws IllegalSyntaxException {
		if((strToCheck.startsWith(strToSearch)) || (strToCheck.endsWith(strToSearch))) {
			return false;
		}
		return true;
	}
}