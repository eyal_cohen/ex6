package oop.ex6.sjava.expression;

import oop.ex6.sjava.CompileErrorException;

/**
 * Thrown when an assignment to a final variable is performed.
 *
 */
public class FinalVariableAsignmentException extends CompileErrorException {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * @param message The information about the reason that the exception was thrown.
	 */
	public FinalVariableAsignmentException(String message) {
		super(message);
	}

}
