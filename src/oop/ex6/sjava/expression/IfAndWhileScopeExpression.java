package oop.ex6.sjava.expression;

import java.util.List;

import oop.ex6.sjava.CompileErrorException;
import oop.ex6.sjava.IfAndWhileScope;
import oop.ex6.sjava.Scope;
import oop.ex6.sjava.Variable;
import oop.ex6.sjava.types.TypeFactory;
import oop.ex6.sjava.types.Type;

/*
 * An expression for a start of an If block or a While block.
 *
 */
class IfAndWhileScopeExpression extends Expression {

	private final static String EXCEPTION_MSG_1 = "Can't use If and While statements in global scope.";
	private final static String EXCEPTION_MSG_2 = "The variable '";
	private final static String EXCEPTION_MSG_3 = "' has not been initialized.";
	private final static String EXCEPTION_MSG_4 = "Cannot convert from ";
	private final static String EXCEPTION_MSG_5 = " to boolean.";
	private final static String EXCEPTION_MSG_6 = "' doesn't exist.";
	
	private List<String> conditionsList;

	/*
	 * Constructor
	 * @param conditions The boolean expressions that are in the brackets where the scope is opened.
	 */
	IfAndWhileScopeExpression(List<String> conditions) {
		this.conditionsList = conditions;
	}
	
	@Override
	public boolean isScopeStart() {
		return true;
	}
	
	@Override
	public void executeExpression(Scope scope) throws CompileErrorException {
		if (scope.getOuterScope() == null) { // performing on a scope without an outer scope- meaning global
			throw new IllegalOperationException(EXCEPTION_MSG_1);
		}
		Variable var;
		Type booleanType = TypeFactory.createVariableType(TypeFactory.BOOLEAN);
		for (String condition : conditionsList) { // going over all the conditions
			if (!booleanType.canAsign(condition)) { // not a direct value, meaning its another variable
				var = scope.findVariable(condition);
				if (var != null) {
					if (!var.isInitialized()) { // assigned an uninitialized variable
						throw new UninitializedVariableAssignmentException(EXCEPTION_MSG_2
								+ var.getName() + EXCEPTION_MSG_3);
					}
					if (!booleanType.canAsign(var.getType())) { // the type cannot be assigned to boolean
						throw new TypeMismatchException(EXCEPTION_MSG_4
								+ var.getType().getStringRepresentation() + EXCEPTION_MSG_5);
					}
					// Variable exists and it's type match.
				} else {
					throw new VariableDoesntExistException(EXCEPTION_MSG_2 + condition + EXCEPTION_MSG_6);
				}
			}
			// It's a valid boolean value.
		}
		IfAndWhileScope newScope = new IfAndWhileScope(scope);
		scope.setInnerScope(newScope);
	}

}
