package oop.ex6.sjava.expression;

import oop.ex6.sjava.CompileErrorException;

/**
 * Thrown when an expression is trying to execute an illegal command (eg. if/while in a global scope).
 *
 */
public class IllegalOperationException extends CompileErrorException {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * @param message The information about the reason that the exception was thrown.
	 */
	public IllegalOperationException(String message) {
		super(message);
	}

}
