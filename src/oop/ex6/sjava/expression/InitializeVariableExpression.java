package oop.ex6.sjava.expression;

import oop.ex6.sjava.CompileErrorException;
import oop.ex6.sjava.Scope;
import oop.ex6.sjava.Variable;
import oop.ex6.sjava.types.Type;

/*
 * An expression for an assignment of a value into a variable. 
 *
 */
class InitializeVariableExpression extends AssignmentsCheckExpression {

	private static final String EXCEPTION_MSG_1 = " can't be resolved to a variable";
	private static final String EXCEPTION_MSG_2 = "The final variable '";
	private static final String EXCEPTION_MSG_3 = "' cannot be reassigned.";
	
	private final String val;
	private final String name;
	
	/*
	 * Constructor.
	 * @param name The name of variable that is being assigned into.
	 * @param val The value to assign.
	 */
	InitializeVariableExpression(String name, String val) {
		this.val = val;
		this.name = name;
	}

	@Override
	public void executeExpression(Scope scope) throws CompileErrorException {
		Variable var = scope.findVariable(this.name); // find the variable in memory. close scope first.
		if(var == null) {
			// if var wasn't found - throw exception
			throw new VariableDoesntExistException(this.name + EXCEPTION_MSG_1);
		}
		if(var.isFinal() && var.isInitialized()) {
			// if var found but it's final and already been initialized - throw exception
			throw new FinalVariableAsignmentException(EXCEPTION_MSG_2 + var.getName() + EXCEPTION_MSG_3);
		}
		Type varType = var.getType();
		// check if it's a legal assignment, or else exception thrown
		assignmentCheck(scope, this.val, varType);
		// do the actual initialization
		var.Initialize();
	}

}
