package oop.ex6.sjava.expression;

import java.util.List;

import oop.ex6.sjava.CompileErrorException;
import oop.ex6.sjava.Method;
import oop.ex6.sjava.Scope;
import oop.ex6.sjava.types.Type;

/*
 * An expression for a method call command.
 *
 */
class MethodCallExpression extends AssignmentsCheckExpression {

	private final static String EXCEPTION_MSG_1 = "The method ";
	private final static String EXCEPTION_MSG_2 = " is undefined.";
	private final static String EXCEPTION_MSG_3 = " receives ";
	private final static String EXCEPTION_MSG_4 = " parameters, but is called with ";
	private final static String EXCEPTION_MSG_5 = " parameters.";
	
	private final String methodName;
	private final List<String> parameters;

	/*
	 * Constructor.
	 * @param name The name of the method being called.
	 * @param parameters The parameters that the method is called with.
	 */
	MethodCallExpression(String name, List<String> parameters) {
		this.methodName = name;
		this.parameters = parameters;
	}

	@Override
	public void executeExpression(Scope scope) throws CompileErrorException {
		Method method = scope.findMethod(this.methodName);
		if (method == null) { // no such method exists
			throw new MethodDoesntExistException(EXCEPTION_MSG_1 + this.methodName + EXCEPTION_MSG_2);
		}
		// We found the correct method:
		List<Type> typesList = method.getParameters();
		if (typesList.size() != this.parameters.size()) { // incorrect number of parameters
			throw new WrongParametersAmountException(EXCEPTION_MSG_1 + this.methodName + EXCEPTION_MSG_3 
					+ typesList.size() + EXCEPTION_MSG_4 + this.parameters.size() + EXCEPTION_MSG_5);
		}
		int parametersNum = this.parameters.size();
		for(int i = 0; i < parametersNum; i++) { // check that each parameter is a legal value
			assignmentCheck(scope, this.parameters.get(i), typesList.get(i));
		}
	}
}
