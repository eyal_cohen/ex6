package oop.ex6.sjava.expression;

import java.util.List;
import oop.ex6.sjava.CompileErrorException;
import oop.ex6.sjava.InvalidNameException;
import oop.ex6.sjava.Method;
import oop.ex6.sjava.Scope;
import oop.ex6.sjava.SjavaRules;
import oop.ex6.sjava.types.Type;
import oop.ex6.sjava.types.TypeDoesntExistException;
import oop.ex6.sjava.types.TypeFactory;

/*
 * An expression for a method declaration.
 *
 */
class MethodDeclarationExpression extends Expression {
	
	private final static String EXCEPTION_MSG_1 = "Tried to initialize a type that doesn't exist in the ";
	private final static String EXCEPTION_MSG_2 = "method ";
	private final static String EXCEPTION_MSG_3 = "'s parameters: ";
	private final static String EXCEPTION_MSG_4 = " is an invalid name for a method.";
	private final static String PERIOD = ".";
	
	private final String name;
	private final String returnType;
	private List<Expression> parametersCreationExpressions;
	
	/*
	 * Constructor
	 * @param methodName The name of the new method.
	 * @param methodReturnType The return type of the new method
	 * @param parametersCreationExpressions A list of expressions that create the methods parameters.
	 */
	MethodDeclarationExpression(String methodName, String methodReturnType, 
												List<Expression> parametersCreationExpressions) {
		this.name = methodName;
		this.returnType = methodReturnType;
		this.parametersCreationExpressions = parametersCreationExpressions;
	}
	
	@Override
	public boolean isScopeStart() {
		return true;
	}
	
	@Override
	public boolean isMethodDecleration() {
		return true;
	}
	
	@Override
	public void executeExpression(Scope scope) throws CompileErrorException {
		Type methodType;
		try { // create the return type of the method
			methodType = TypeFactory.createMethodType(this.returnType);
		} catch (TypeDoesntExistException e) { // no such type for a method
			throw new TypeDoesntExistException(EXCEPTION_MSG_1
					+ EXCEPTION_MSG_2 + this.name + EXCEPTION_MSG_3 + this.returnType + PERIOD);
		}
		if(!SjavaRules.isLegalMethodName(this.name)) { // if the name of the method is illegal
			throw new InvalidNameException(this.name + EXCEPTION_MSG_4);
		}
		// all is good, so create the new method object:
		Method method = new Method(methodType, this.name, this.parametersCreationExpressions.size(), scope);
		for(Expression expression : parametersCreationExpressions) {
			// for each expression in the parameters create the actual variable in the new method
			expression.executeExpression(method);
		}
		// add new method to its outer scope
		scope.addMethod(method);
	}

}
