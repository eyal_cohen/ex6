package oop.ex6.sjava.expression;

import oop.ex6.sjava.CompileErrorException;

/**
 * Thrown when a call to an undeclared method is performed.
 *
 */
public class MethodDoesntExistException extends CompileErrorException {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor
	 * @param message The information about the reason that the exception was thrown.
	 */
	MethodDoesntExistException(String message) {
		super(message);
	}
}
