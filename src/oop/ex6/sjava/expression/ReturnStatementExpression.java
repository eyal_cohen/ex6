package oop.ex6.sjava.expression;

import oop.ex6.sjava.CompileErrorException;
import oop.ex6.sjava.Scope;

/*
 * An expression for a return statement in a sjava program.
 *
 */
class ReturnStatementExpression extends Expression {
	
	@Override
	public void executeExpression(Scope scope)
			throws CompileErrorException {
		// signals to it's containing scope that it can end now.
		scope.setCanEnd(true);
	}

}
