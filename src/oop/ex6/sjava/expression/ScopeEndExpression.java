package oop.ex6.sjava.expression;

import oop.ex6.sjava.CompileErrorException;
import oop.ex6.sjava.Scope;
import oop.ex6.sjava.UnbalancedScopesException;

/*
 * An expression for end of scope in a sjava program.
 *
 */
class ScopeEndExpression extends Expression {
	
	private final static String EXCEPTION_MSG_1 = "Can't end unopened scope";
	private final static String EXCEPTION_MSG_2 = "Can't end method without return statement.";
	
	@Override
	public boolean isScopeEnd() {
		return true;
	}
	
	@Override
	public void executeExpression(Scope scope)
			throws CompileErrorException {
		if(scope.getOuterScope() == null) {
			// can't close global scope
			throw new UnbalancedScopesException(EXCEPTION_MSG_1);
		}
		if(!scope.canEnd()) {
			// scope can't end (i.e. no return in a method)
			throw new CantEndScopeException(EXCEPTION_MSG_2);
		}
		// close the scope
		Scope outerScope = scope.getOuterScope();
		outerScope.setInnerScope(null);
	}

}
