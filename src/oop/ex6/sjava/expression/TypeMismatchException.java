package oop.ex6.sjava.expression;

import oop.ex6.sjava.CompileErrorException;

/**
 * Thrown when trying to assign a value to a variable, that cannot be assigned to it because of
 * types considerations.
 *
 */
public class TypeMismatchException extends CompileErrorException {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * @param message The information about the reason that the exception was thrown.
	 */
	public TypeMismatchException(String message) {
		super(message);
	}

}
