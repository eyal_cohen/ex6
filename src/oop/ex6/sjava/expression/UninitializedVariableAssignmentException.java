package oop.ex6.sjava.expression;

import oop.ex6.sjava.CompileErrorException;

/**
 * Thrown when a variable which wasn't initialized yet is assigned to another variable.
 *
 */
public class UninitializedVariableAssignmentException extends
		CompileErrorException {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor
	 * @param message The information about the reason that the exception was thrown.
	 */
	public UninitializedVariableAssignmentException(String message) {
		super(message);
	}
	
}
