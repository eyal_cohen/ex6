package oop.ex6.sjava.expression;

import oop.ex6.sjava.CompileErrorException;

/**
 * Thrown when trying to use a variable that doesn't exist.
 *
 */
public class VariableDoesntExistException extends CompileErrorException {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * @param message The information about the reason that the exception was thrown.
	 */
	public VariableDoesntExistException(String message) {
		super(message);
	}
	
}