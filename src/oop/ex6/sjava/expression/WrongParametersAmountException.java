package oop.ex6.sjava.expression;

import oop.ex6.sjava.CompileErrorException;

/**
 * Thrown when a method is called with a wrong number of parameters.
 *
 */
public class WrongParametersAmountException extends CompileErrorException {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor
	 * @param message The information about the reason that the exception was thrown.
	 */
	WrongParametersAmountException(String message) {
		super(message);
	}
}
