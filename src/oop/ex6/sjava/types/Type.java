package oop.ex6.sjava.types;

import java.util.Arrays;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oop.ex6.sjava.SjavaRules;

/**
 * A class that represents a type in a sjava program. (variable types and method return types)
 *
 */
public enum Type {
	
	// For each type, three parameters are needed :
		// 1. name of the type (taken from TypeFactory).
		// 2. regex pattern of legal value.
		// 3. Array of strings, representing the valid types to put in this type.
	
	INT (
			TypeFactory.INT,
			"-?[\\d]+",
			new String[] {TypeFactory.INT}
		),
	DOUBLE (
			TypeFactory.DOUBLE,
			"-?\\d+(?:\\.\\d+)?",
			new String[] {TypeFactory.DOUBLE, TypeFactory.INT}
		),
	CHAR (
			TypeFactory.CHAR,
			"'.'",
			new String[] {TypeFactory.CHAR}
		),
	STRING (
			TypeFactory.STRING,
			"\".*\"",
			new String[] {TypeFactory.STRING}
		),
	BOOLEAN (
			TypeFactory.BOOLEAN,
			"(?:-?\\d+(?:\\.\\d+)?|(?:" + SjavaRules.TRUE + "|" + SjavaRules.FALSE + "))", 
			new String[] {TypeFactory.BOOLEAN, TypeFactory.INT, TypeFactory.DOUBLE}
		),
	VOID (
			TypeFactory.VOID,
			"", 
			new String[] {}
		);
	
	String typeName;
	String regexPattern;
	HashSet<String> allowedTypesSet;
	Pattern pattern;
	
	/**
	 * Constructor for enum Type. Assigning values and compiling regex pattern for each type.
	 * @param typeName The name of the specific type.
	 * @param regexPattern The regex for legal values being assigned to the type.
	 * @param allowedAssignType Other types that can be assigned to this type.
	 */
	Type(String typeName, String regexPattern, String[] allowedAssignType) {
		this.typeName = typeName;
		this.regexPattern = regexPattern;
		this.allowedTypesSet = new HashSet<String>(Arrays.asList(allowedAssignType));
		this.pattern = Pattern.compile(this.regexPattern);
	}
	
	/**
	 * Is it allowed to assign the given type to a variable of this type?
	 * @param type The given type.
	 * @return true iff it can be assigned.
	 */
	public boolean canAsign(Type type) {
		String typeString = type.getStringRepresentation();
		if(allowedTypesSet.contains(typeString)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Is it allowed to assign the given string to a variable of this type?
	 * @param val The value that is being assigned.
	 * @return true iff the value can be assigned to this type.
	 */
	public boolean canAsign(String val) {
		Matcher matcher = this.pattern.matcher(val);
		if(matcher.matches()) {
			return true;
		}
		return false;
	}
	
	/**
	 * Returns the string representation of the specific type, for example:
	 * "int" for integer, "char" for single characters, etc.
	 * 
	 * @return The string representation of the type.
	 */
	public String getStringRepresentation() {
		return this.typeName;
	}

}