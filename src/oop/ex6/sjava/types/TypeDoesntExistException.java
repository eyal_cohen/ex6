package oop.ex6.sjava.types;

import oop.ex6.sjava.CompileErrorException;

/**
 * Thrown when trying to create a non existing type.
 *
 */
public class TypeDoesntExistException extends CompileErrorException {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor
	 * @param message The information about the reason that the exception was thrown.
	 */
	public TypeDoesntExistException(String message){
		super(message);
	}
}