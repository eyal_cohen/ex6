package oop.ex6.sjava.types;

/**
 * A class that is used to instantiate 'Type' objects.
 * The class has 2 methods for creating types: createVariableType, and createMethodType- for creating variable
 * and method types respectively.
 *
 */
public class TypeFactory {

	// Static name for each type
	public static final String INT = "int";
	public static final String DOUBLE = "double";
	public static final String BOOLEAN = "boolean";
	public static final String STRING = "String";
	public static final String CHAR = "char";
	public static final String VOID = "void";

	private static final String EXCEPTION_MSG_1 = "Tried to declare a variable of a "
			+ "type that doesn't exist: ";
	private static final String PERIOD = ".";

	/**
	 * Creates a variable type object from a string.
	 * 
	 * @param typeString The string representing the type.
	 * @return Type object, according to the typeString that was received.
	 * @throws TypeDoesntExistException if there is no type corresponding with the string received.
	 */
	public static Type createVariableType(String typeString)
			throws TypeDoesntExistException {
		switch (typeString) {
		case INT:
			return Type.INT;
		case DOUBLE:
			return Type.DOUBLE;
		case BOOLEAN:
			return Type.BOOLEAN;
		case STRING:
			return Type.STRING;
		case CHAR:
			return Type.CHAR;
		default:
			throw new TypeDoesntExistException(EXCEPTION_MSG_1 + typeString + PERIOD);
		}
	}

	/**
	 * Creates a method return type object from a string.
	 * 
	 * @param typeString The string representing the type.
	 * @return Type object, according to the typeString that was received.
	 * @throws TypeDoesntExistException if there is no type corresponding with the string received.
	 */
	public static Type createMethodType(String typeString)
			throws TypeDoesntExistException {
		switch (typeString) {
		case VOID:
			return Type.VOID;
		default:
			throw new TypeDoesntExistException(EXCEPTION_MSG_1 + typeString + PERIOD);
		}
	}
}
